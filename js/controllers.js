var trainingControllers = angular.module('trainingControllers', []);

trainingControllers.controller('AnimalsCtrl', function($scope) {
  $scope.order = 'id';
  $scope.animals = [
    {
      id: 1,
      type: 'Chien',
      nom: 'Harry',
      pattes: 4
    }, {
      id: 2,
      type: 'Chat',
      nom: 'Felix',
      pattes: 4
    }, {
      id: 3,
      type: 'Arraignée',
      nom: 'Edouard',
      pattes: 8
    }, {
      id: 4,
      type: 'Poule',
      nom: 'Anna',
      pattes: 2
    }, {
      id: 5,
      type: 'Mille-patte',
      nom: 'Flash',
      pattes: 1000
    }, {
      id: 6,
      type: 'Fourmi',
      nom: 'Samantha',
      pattes: 6
    }
  ]
});

trainingControllers.controller('CalculCtrl', function($scope) {
  $scope.nb1 = 1;
  $scope.nb2 = 2;
  $scope.result = $scope.nb1 + $scope.nb2;

  $scope.addition = function() {
    $scope.result = $scope.nb1 + $scope.nb2;
  };
});
