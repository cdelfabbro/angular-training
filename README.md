# Angular-training #

Ce programme permet de découvrir les notions de bases d'AngularJS essentiellement tournées autour des directives. Il vous permettra de faire une courte présentation de l'utilité et la simplicité 
que peut apporter AngularJS.

### Technologies utilisées ###

* [HTML5](http://www.w3schools.com/html/html5_intro.asp)
* [Bootstrap 3.2](http://getbootstrap.com/)
* [Thème Flatly](http://bootswatch.com/flatly/)
* [AngularJS](https://angularjs.org/)

### Installation ###

```
git clone https://bitbucket.org/cdelfabbro/angular-training.git
```
### Utilisation ###

Le projet est divisé en plusieurs step d'avancement pour présenter différentes directives d'angularJS.
Pour changer de step, vous devrez spécifier le step que vous souhaitez voir.

* Step 0 - Chargement d'AngularJS
```
git checkout -f step-0
```
* Step 1 - Utilisation des directives ng-app, ng-model, ng-repeat
```
git checkout -f step-1
```
* Step 2 - Utilisation de la directive ng-controller, notion de scope (modèle) et utilisation des filtres
```
git checkout -f step-2
```
* Step 3 - Utilisation des directives ng-show et ng-hide, implémentation du module ngAnimate
```
git checkout -f step-3
```
* Step 4 - Utilisation de la directive ng-click, création méthode d'un contrôleur, utilisation des modules pour structurer son code.
```
git checkout -f step-4
```

### Inspiration ###

* [AngularJS Tutoriel](https://docs.angularjs.org/tutorial)
* [Grafikart](http://www.grafikart.fr/formation/angularjs)